%% (c) 2006 RIVAUD Julien
%% License: GPL 2.0 or greater
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\RequirePackage{expl3}
\ProvidesExplClass{simple}{2010/10/03}{2.0}{Minimal document class}

\RequirePackage{xparse}
\RequirePackage{l3keys2e}

% Helper functions to define font size commands

\tl_new:N \l_rivsimple_baselineratio_tl
\tl_new:N \l_rivsimple_normalsize_tl

\cs_new_nopar:Nn \rivsimple_declare_size_command:Nn
{
    \dim_set:Nn \l_tmpa_dim { #2 }
    \dim_set:Nn \l_tmpb_dim
        { \l_tmpa_dim * \l_rivsimple_baselineratio_tl / 100 }
    \tl_set:Nx \l_tmpa_tl
    {
        \exp_not:n { \DeclareDocumentCommand #1 { } }
        {
            % TODO: \lineskiplimit = f(\lineskip)
            \exp_not:n { \@setfontsize #1 }
                { \dim_use:N \l_tmpa_dim }{ \dim_use:N \l_tmpb_dim }
        }
    }
    \tl_use:N \l_tmpa_tl
}

\cs_new_nopar:Nn \rivsimple_setup_sizes:n
{
    % For code depending on article-like setup of font sizes
    \dim_compare:nNnTF { #1 } < { 10.5pt } {
        \tl_set:Nn \@ptsize {0}
    }{\dim_compare:nNnTF { #1 } < { 11.5pt } {
        \tl_set:Nn \@ptsize {1}
    }{
        \tl_set:Nn \@ptsize {2}
    }}
    % Define the font size commands
    \rivsimple_declare_size_command:Nn \tiny
        { #1 * 58 / 100 }
    \rivsimple_declare_size_command:Nn \scriptsize
        { #1 * 68 / 100 }
    \rivsimple_declare_size_command:Nn \footnotesize
        { #1 * 78 / 100 }
    \rivsimple_declare_size_command:Nn \small
        { #1 * 88 / 100 }
    \rivsimple_declare_size_command:Nn \normalsize
        { #1 }
    \rivsimple_declare_size_command:Nn \large
        { #1 * 12 / 10 }
    \rivsimple_declare_size_command:Nn \Large
        { #1 * 12 / 10 * 12 / 10 }
    \rivsimple_declare_size_command:Nn \LARGE
        { #1 * 12 / 10 * 12 / 10 * 12 / 10 }
    \rivsimple_declare_size_command:Nn \huge
        { #1 * 12 / 10 * 12 / 10 * 12 / 10 * 12 / 10 }
    \rivsimple_declare_size_command:Nn \Huge
        { #1 * 12 / 10 * 12 / 10 * 12 / 10 * 12 / 10 * 12 / 10 }
}

% Process options

\keys_define:nn { rivsimple/classoptions }
{
    baselineratio   .value_required:n = true,
    baselineratio   .tl_set:N = \l_rivsimple_baselineratio_tl,
    normalsize      .value_required:n = true,
    normalsize      .tl_set:N = \l_rivsimple_normalsize_tl,
    oldfontcommands .value_forbidden:n = true,
    oldfontcommands .code:n =
        \DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
        \DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
        \DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
        \DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
        \DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
        \DeclareOldFontCommand{\sl}{\normalfont\slshape}{\@nomath\sl}
        \DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}
}

\prg_stepwise_inline:nnnn { 8 } { 1 } { 13 }
{
    \keys_define:nn { rivsimple/classoptions }
    {
        #1pt .value_forbidden:n = true,
        #1pt .code:n = \tl_set:Nn \l_rivsimple_normalsize_tl { #1pt }
    }

}

\keys_set:nn { rivsimple/classoptions } { baselineratio=124, 11pt }
\ProcessKeysOptions { rivsimple/classoptions }

\exp_args:NV \rivsimple_setup_sizes:n \l_rivsimple_normalsize_tl
\normalsize

% TODO: Cleanup ?

\endinput


