%% (c) 2006 RIVAUD Julien
%% License: GPL 2.0 or greater
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{newsp}[2006/09/15 Common setup for my articles]

\LoadClass{simple}

\RequirePackage{perso}

\RequirePackage[paper=a4paper,margin={2cm,2.5cm}]{geometry}
\RequirePackage{multicol}

\RequirePackage{calc}

\def\sign#1#2{\begin{flushright}{\slshape\sffamily#1}, #2\end{flushright}}

\clubpenalty 2500
\widowpenalty 5000
\parskip .5ex plus .2ex minus .1ex
\parindent 2em

\setlength{\columnsep}{2\baselineskip+\parskip-1ex}



\def\article#1{\begin{multicols}{2}[%
    \begingroup\centering\Large\bfseries#1%
    \vskip 1cm\relax\endgroup
]}

\def\endarticle{\end{multicols}}

