%% (c) 2006 RIVAUD Julien
%% License: GPL 2.0 or greater
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\RequirePackage{expl3}
\ProvidesExplClass{rivexos}{2012/12/07}{1.0}{Class for exercise sheets}

\RequirePackage{xparse}
\RequirePackage{l3keys2e}

\LoadClassWithOptions{rivexam}
\rivexamsetup{ending=none}

\RequirePackage{geometry}
\geometry{margin=1.5cm}

\RequirePackage{rivghost}

%%% Options %

% Registers
\int_new:N      \g_rivexos_columncount_int

\keys_define:nn { rivexos/classoptions }
{
    columncount     .value_required:n = true,
    columncount     .int_gset:N         = \g_rivexos_columncount_int,
}

\keys_set:nn { rivexos/classoptions }
{
    columncount     = 2,
}

\ProcessKeysOptions { rivexam/classoptions }

\NewDocumentCommand \rivexossetup { m } {
    \keys_set_known:nnN { rivexos/classoptions } { #1 } \l_tmpa_tl
    \exp_args:NV \rivbooksetup \l_tmpa_tl
}


%%% Structure %

\RequirePackage{etoolbox}
\RequirePackage{multicol}

\dim_set:Nn \columnsep      {1.9cm}
\dim_set:Nn \columnseprule  {.4pt}


\AtEndPreamble{\AtBeginDocument{
    \hrule height 0pt\relax
    \begin{multicols}{\g_rivexos_columncount_int}
}}
\AtEndDocument{
    \end{multicols}
}


%% Exos %

\newcounter{exo}[section]
\RenewDocumentCommand \exo { s G{} } {
    \par
    \penalty 0\relax
    \addvspace { 1.5ex plus 1ex minus 0.5ex }
    \group_begin:
        \bfseries
        \raggedright
        \noindent
        \skip_zero:N \parfillskip
        \Exoname\nobreak\space
        \IfBooleanF{#1}{
            \refstepcounter{exo}
            \theexo
            \tl_if_empty:nF{#2}{\exosep}
        }
        {#2}\par
    \group_end:
    \nobreak
    \addvspace{.2ex}
    \@nobreaktrue
    \@afterheading
}

\tl_set:Nn \theexo  {\arabic{exo}}
\tl_new:N  \Exoname
\tl_set:Nn \Exoname {Exercise}
\tl_new:N  \exosep
\tl_set:Nn \exosep  {~--~}

\AtEndPreamble{
    \tl_if_exist:NT \captionsfrench {
        \addto\captionsfrench{
            \def\Exoname{Exercice}
        }
    }
}


%% New exercise interface %

\NewDocumentEnvironment{exercise}{O{}}{
    \exo{#1}
}{
    \par
}

% Auto exam number
\tl_const:Nx \c__persocours_chars_tl {
    \tl_to_str:n { ABCDEFGHIJKLMNOPQRSTUVWXYZ }
}
\tl_const:Nx \c__persocours_nums_tl {
    \tl_to_str:n { 0123456789 }
}

\tl_new:N \l_persocours_num_tl
\tl_new:N \l_persocours_prefix_tl
\tl_set:Nx \l_tmpa_tl { \tl_to_str:N \jobname }

\tl_map_inline:Nn \l_tmpa_tl {
    \tl_if_in:NnF \c__persocours_chars_tl { #1 } { \tl_map_break: }
    \tl_put_right:Nn \l_persocours_prefix_tl { #1 }
    \tl_set:Nx \l_tmpa_tl { \tl_tail:N \l_tmpa_tl }
}
\tl_map_inline:Nn \l_tmpa_tl {
    \tl_if_in:NnF \c__persocours_nums_tl { #1 } { \tl_map_break: }
    \tl_put_right:Nn \l_persocours_num_tl { #1 }
}
\tl_if_empty:NF \l_persocours_num_tl {
    \setcounter{chapter}{\l_persocours_num_tl}
    \tl_if_empty:NF \l_persocours_prefix_tl {
        \tl_set:Nn \thechapter
                { \l_persocours_prefix_tl \arabic{chapter} }
    }
}


%%% Enums

\setlist[enumerate,1]{label=\arabic*.,format=\textbf}
\setlist[enumerate,2]{
    label={\itshape\alph*\/\upshape)},
    ref=\theenumi.\thinspace{\itshape\alph*\/\upshape)},
    widest=a,
}%
\RenewDocumentEnvironment{questions}{}{\begin{enumerate}}{\end{enumerate}}
