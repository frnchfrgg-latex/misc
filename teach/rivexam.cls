%% (c) 2013 RIVAUD Julien
%% License: GPL 2.0 or greater
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\RequirePackage{expl3}
\ProvidesExplClass{rivexam}{2013/01/20}{2.0}{Class for exams and tests}

\RequirePackage{xparse}
\RequirePackage{l3keys2e}

\RequirePackage{etoolbox}
\AtEndPreamble{\RequirePackage{cleveref}}%after varioref

\LoadClass{rivbook}
%%% Options %

% Registers
\tl_new:N \g_rivexam_ending_tl
\clist_new:N \l_rivexam_rivbookoptions_clist

% Keys
\keys_define:nn { rivexam / classoptions } {
    pagesetup       .default:n          = true,
    pagesetup       .bool_set:N         = \l_rivexam_pagesetup_bool,

    maketitle       .default:n          = true,
    maketitle       .bool_set:N         = \l_rivexam_maketitle_bool,

    titlestyle      .value_required:n = true,
    titlestyle      .tl_gset:N          = \g_rivexam_titlestyle_tl,
    titlestyle      .initial:n          = \huge\scshape,

    titlewidth      .value_required:n = true,
    titlewidth      .tl_gset:N          = \g_rivexam_titlewidth_tl,
    titlewidth      .initial:n          = 28em,

    titleinnersep   .value_required:n = true,
    titleinnersep   .tl_gset:N          = \g_rivexam_titleinnersep_tl,
    titleinnersep   .initial:n          = 4ex,

    subtitlestyle   .value_required:n = true,
    subtitlestyle   .tl_gset:N          = \g_rivexam_subtitlestyle_tl,
    subtitlestyle   .initial:n          = \large,

    introstyle      .value_required:n = true,
    introstyle      .tl_gset:N          = \g_rivexam_introstyle_tl,

    introwidth      .value_required:n = true,
    introwidth      .tl_gset:N          = \g_rivexam_introwidth_tl,

    titlebottomsep  .value_required:n = true,
    titlebottomsep  .tl_gset:N          = \g_rivexam_titlebottomsep_tl,
    titlebottomsep  .initial:n          = 6ex plus 6ex minus 3ex,

    endingsep       .value_required:n = true,
    endingsep       .skip_gset:N        = \g_rivexam_endingsep_skip,

    ending          .value_required:n = true,
    ending          .meta:nn            = { rivexam / ending }{ #1 },

    exos            .value_required:n = true,
    exos            .meta:nn            = { rivexam / exo }{ #1 },

    levels          .value_required:n = true,
    levels          .meta:nn            = { rivexam / levels }{ #1 },

    unknown         .code:n =
                \clist_put_right:Nx \l_rivexam_rivbookoptions_clist
                        {
                            \l_keys_key_tl
                            \bool_if:NF \l__keys_no_value_bool {
                                = \exp_not:n {#1}
                            }
                        },
}

\keys_define:nn { rivexam / ending } {
    none    .value_forbidden:n = true,
    none    .code:n             = \tl_gclear:N \g_rivexam_ending_tl,

    stars   .default:n          = \large,
    stars   .code:n             =
            \tl_gset:Nn \g_rivexam_ending_tl { \rivexam_ending_stars:n{#1} },

    text   .value_required:n = true,
    text   .code:n             =
            \tl_gset:Nn \g_rivexam_ending_tl { \rivexam_ending_text:n{#1} },
}


\keys_set:nn { rivexam/classoptions } {
    maketitle       = true,
    introstyle      = \small,
    introwidth      = 29em,
    endingsep       = 2cm minus 1.5cm,
    pagesetup       = true,
    ending          = stars,
}

\ProcessKeysOptions { rivexam/classoptions }
\exp_args:NV \rivbooksetup \l_rivexam_rivbookoptions_clist [\l_tmpa_tl]
\tl_gset_eq:NN \@unusedoptionslist \l_tmpa_tl
\bool_if:NT \l_rivexam_pagesetup_bool {
    \RequirePackage{geometry}
}

\NewDocumentCommand \rivexamsetup { m } {
    \clist_clear:N \l_rivexam_rivbookoptions_clist
    \keys_set:nn { rivexam/classoptions } { #1 }
    \exp_args:NV \rivbooksetup \l_rivexam_rivbookoptions_clist
}

\cs_new_nopar:Nn  \rivexam_dim_setup: {
    \bool_if:NT \l_rivexam_pagesetup_bool {
        \geometry{
            a4paper,
            margin=2cm,
            includeheadfoot,
            bindingoffset=0pt,
        }
    }
    \dim_zero:N \parindent
    \dim_set:Nn \columnsep {3em}
}

\rivbook_addto_dimenhook:n {
    \rivexam_dim_setup:
}

\rivexam_dim_setup:


%%% Headings and footings content %
\renewpagestyle{headings}{
    \setfoot{}{---\nobreakspace\thepage\nobreakspace---}{}
    \sethead
        {\small\tl_use:N\g_rivexam_class_tl}
        {\small\tl_use:N\shorttitle}
        {\small\tl_use:N\@date}
}
\newpagestyle{firstpage}{
    \setfoot{}{---\nobreakspace\thepage\nobreakspace---}{}
    \sethead
        {\small\tl_use:N\g_rivexam_class_tl}
        {}
        {\small\tl_use:N\@date}
}
\pagestyle{headings}


%%% Structure %

\RenewDocumentCommand \subtitle {m} {\tl_set:Nn \g_rivexam_subtitle_tl {#1}}
\NewDocumentCommand   \class    {m} {\tl_set:Nn \g_rivexam_class_tl {#1}}
\NewDocumentCommand   \intro    {m} {\tl_set:Nn \g_rivexam_intro_tl {#1}}

\subtitle{}
\class{}
\intro{}

\bool_new:N \g__rivexam_dobottomsep_bool

\RenewDocumentCommand \maketitle {} {
    \thispagestyle{firstpage}
    \suppressfloats[t] % We do not want figures or table above the title
    \vbox:n {
    \centering
    \tl_if_empty:NF \longtitle {
        \bool_gset_true:N \g__rivexam_dobottomsep_bool
        \tl_use:N \g_rivexam_titlestyle_tl
        \skip_set:Nn \leftskip {
                (\textwidth - \g_rivexam_titlewidth_tl) / 2
                + 0pt plus 1fill
        }
        \skip_set_eq:NN \rightskip \leftskip
        \tl_use:N \longtitle
        \par
    }
    \tl_if_empty:NF \g_rivexam_subtitle_tl {
        \bool_gset_true:N \g__rivexam_dobottomsep_bool
        \normalfont\normalsize
        \skip_vertical:n {\g_rivexam_titleinnersep_tl}
        \tl_use:N \g_rivexam_subtitlestyle_tl
        \tl_use:N \g_rivexam_subtitle_tl
        \par
    }
    \tl_if_empty:NF \g_rivexam_intro_tl {
        \bool_gset_true:N \g__rivexam_dobottomsep_bool
        \normalfont\normalsize
        \skip_vertical:n {\g_rivexam_titleinnersep_tl}
        \tl_use:N \g_rivexam_introstyle_tl
        \skip_set:Nn \leftskip
            { (\textwidth - \g_rivexam_introwidth_tl) / 2 + 0pt plus 1fill }
        \skip_set_eq:NN \rightskip \leftskip
        \exp_args:NnV \tl_rescan:nn {} \g_rivexam_intro_tl
        \par
    }
    }
    \bool_if:NT \g__rivexam_dobottomsep_bool {
        \skip_vertical:n {\g_rivexam_titlebottomsep_tl}
    }
}


\AtEndPreamble{\AtBeginDocument{
    \bool_if:NT \l_rivexam_maketitle_bool { \maketitle }
}}

\AtEndDocument{
    \tl_if_empty:NF \g_rivexam_ending_tl {
        \skip_vertical:N \g_rivexam_endingsep_skip
        \vbox:n{
            \tl_use:N \g_rivexam_ending_tl
        }
    }
}

\cs_new_nopar:Nn \rivexam_ending_stars:n {
    \centering
    \tl_use:N #1
    \hbox_to_wd:nn {\textwidth}{
        \hss\textasteriskcentered\quad\textasteriskcentered\hss
    }
    \penalty-10000
    \hbox_to_wd:nn {\textwidth}{
        \hss\textasteriskcentered\hss
    }
    \par
}

\cs_new_nopar:Nn \rivexam_ending_text:n {
    \centering
    \tl_use:N #1
    \par
}


%%% Enum styles %

\setlist[enumerate,1]{
    label=\textup{\textit{\alph*})},
    ref=\thequestionsi.\thinspace\textup{\textit{\alph*})},
    widest*=1,
}%
\setlist[enumerate,2]{
    label=\textup{(\textit{\roman*})},
    ref=\theenumi\thinspace\textup{(\textit{\roman*})},
    widest*=3,
    align=right,
}%

\cs_new_protected:Nn \rivexam_format_question:n { \textbf{#1.} }
\newlist{questions}{enumerate}{1}
\setlist[questions]{
    format=\rivexam_format_question:n,
    label=Question~\arabic*,
    ref=\arabic*,
    labelsep=.4em,
    labelindent=0pt,
    leftmargin=2em,
    itemindent=*,
    itemsep=1.5ex,
    topsep=1.5ex,
}



%% Exos

\RequirePackage{siunitx}
\RequirePackage{fmtcount}

\def\exoname{exercise}

\newcounter{numexo}
\def\thenumexo{\arabic{numexo}}
\keys_define:nn { rivexam / exo } {
    points  .value_required:n = true,
    points  .fp_set:N = \l__rivexam_exo_points_fp,
    pts     .meta:n = { points = #1 },

    headingformat .value_required:n = true,
    headingformat .tl_set:N = \l_rivexam_exoformat_tl,
    headingformat .initial:n = \centering\Large\scshape
                            \Ordinalstring{numexo}\space\exoname,

    pointsformat .value_required:n = true,
    pointsformat .code:n = \cs_set:Nn \__rivexam_pointsformat:n { #1 },
    pointsformat .initial:n = ($\approx\SI{#1}{pts}$),

    partformat .value_required:n = true,
    partformat .tl_set:N = \l_rivexam_exopartformat_tl,
    partformat .initial:n = \large\exopartname\space\thenumexopart,

    aboveskip .value_required:n = true,
    aboveskip .skip_set:N = \exoskip,
    aboveskip .initial:n = 6ex plus 2.5ex minus 2.5ex,

    belowskip .value_required:n = true,
    belowskip .skip_set:N = \l_rivexam_exo_below_skip,
    belowskip .initial:n = 4ex,

    %TODO same for part skips
}
\NewDocumentCommand \exo { O{} } {
    \par
    \refstepcounter{numexo}
    \group_begin:
        % Parse options
        \fp_zero:N \l__rivexam_exo_points_fp
        \keys_set:nn { rivexam / exo } { #1 }
        % Typeset title
        \sectionbreak
        \int_compare:nNnT {\c@numexo} > {1} {
            \nobreak
            \addvspace \exoskip
        }
        \box_clear:N \l_tmpa_box
        \fp_compare:nNnT \l__rivexam_exo_points_fp > 0 {
            \hbox_set:Nn \l_tmpa_box {
                \exp_args:Nx
                    \__rivexam_pointsformat:n
                    {\fp_use:N\l__rivexam_exo_points_fp}
            }
            \iow_now:Nx \@auxout {
                \exp_not:N \@rivexam@setexopoints
                    { \int_use:N \c@numexo }
                    { \fp_to_decimal:N \l__rivexam_exo_points_fp }
            }
        }
        \tl_use:N \l_rivexam_exoformat_tl
        \box_if_empty:NF \l_tmpa_box {
            \null
            \skip_horizontal:n {\parfillskip}
            \skip_horizontal:n {\rightskip}
            \hbox_overlap_left:n{\hbox_unpack_clear:N \l_tmpa_box}
            \skip_zero:N \parfillskip
            \skip_zero:N \rightskip
        }
        \par
    \group_end:
    \nobreak
    \addvspace{\l_rivexam_exo_below_skip}
    \@nobreaktrue
    \@afterheading
}
\clist_new:N \rivexamexos
\fp_new:N \g__rivexam_exos_points_total_fp
\cs_new_nopar:Npn\@rivexam@setexopoints#1#2{
    \clist_gput_right:Nn \rivexamexos {#1}
    \tl_gset:cn{g__rivexam_exo_points_#1_tl}{#2}
    \fp_gadd:Nn \g__rivexam_exos_points_total_fp {#2}
}
\cs_new_nopar:Npn\rivexampoints#1{
    \tl_if_exist:cTF{g__rivexam_exo_points_#1_tl}{
        \tl_use:c{g__rivexam_exo_points_#1_tl}
    }{
        0
    }
}
\cs_new_nopar:Npn\rivexamtotalpoints{
    \fp_to_decimal:N \g__rivexam_exos_points_total_fp
}
\newcounter{numexopart}[numexo]
\def\thenumexopart{\Alph{numexopart}}
\def\exopartname{Part}
\def\exopart{
    \par
    \penalty\c_zero
    \addvspace{0.6ex}
    \refstepcounter{numexopart}
    {
        \tl_use:N \l_rivexam_exopartformat_tl
        \par
    }
    \nobreak
    \addvspace{2ex}
    \@nobreaktrue
    \@afterheading
}

\AtEndPreamble{
    \tl_if_exist:NT \captionsfrench {
        \addto\captionsfrench{
            \def\exoname{exercice}
            \def\exopartname{Partie}
            \crefname{numexo}{l'exercice}{les~exercices}
            \crefname{numexopart}{la~partie}{les~parties}
            \crefname{questionsi}{la~question}{les~questions}
            \crefalias{enumi}{questionsi}
            \crefalias{enumii}{questionsi}
        }
    }
}



%% Multilevel

\RequirePackage{multicol}

\int_new:N \l_rivexam_multilevel_numexo_int

\newcounter{currentlevel}
\tl_new:N   \levelname
\tl_new:N   \currentlevelformat

\tl_set:Nn \levelname    { Niveau }
\tl_set:Nn \thecurrentlevel { \arabic{currentlevel} }

\keys_define:nn { rivexam / levels } {
    headingformat .value_required:n = true,
    headingformat .tl_set:N = \l_rivexam_levelformat_tl,
    headingformat .initial:n =
        \centering\bfseries\levelname{}~\thecurrentlevel,

    columns .value_required:n = true,
    columns .int_set:N = \l_rivexam_multilevel_colcount_int,
    columns .initial:n = 2,

    start .value_required:n = true,
    start .int_set:N = \l_rivexam_multilevel_start_int,
    start .initial:n = 1,
}

\int_step_inline:nnn {1}{9}{
    \keys_define:nn { rivexam / levels } {
        #1 .value_forbidden:n = true,
        #1 .code:n = {
            \int_set:Nn \l_rivexam_multilevel_colcount_int {#1}
        }
    }
}

\NewDocumentCommand \rivexam_nextlevel:w { sso } {
    \par
    \IfValueT{#3}{
        \rivexam_multilevel_close:
        \int_set:Nn \l_rivexam_multilevel_colcount_int {1}
    }
    \IfBooleanTF {#1} {
        \IfBooleanTF {#2} {
            % two stars: always a break
            \int_compare:nNnT {\l_rivexam_multilevel_colcount_int} > {1} {
                \vfill\null\columnbreak
            }{
                \clearpage
            }
        }{
            % one star: never a break
            \nobreak\medskip
        }
    }{
        % no star: break if columns, else tell it is a not-so-bad place
        % to cut a page.
        \int_compare:nNnT {\l_rivexam_multilevel_colcount_int} > {1} {
            \vfill\hrule height 0pt\relax
            \columnbreak
        }{
            \sectionbreak
            \medskip
        }
    }
    \IfValueTF{#3}{
        \int_set:Nn \l_rivexam_multilevel_colcount_int {#3}
        \rivexam_multilevel_open:
    }{
        \rivexam_nextlevel_typeset:
    }
}
\cs_new_nopar:Nn \rivexam_nextlevel_typeset: {
    \int_gset:Nn \c@numexo { \l_rivexam_multilevel_numexo_int }
    \stepcounter{currentlevel}
    {\tl_use:N \l_rivexam_levelformat_tl \par}
    \nobreak
    \@nobreaktrue
    \@afterheading
}

\NewDocumentEnvironment {multilevel} { O{} } {
    \keys_set:nn {rivexam / levels} {#1}
    \cs_set_eq:NN \nextlevel \rivexam_nextlevel:w
    \int_set:Nn \l_rivexam_multilevel_numexo_int { \c@numexo }
    \setcounter{currentlevel}{\int_eval:n{\l_rivexam_multilevel_start_int-1}}
    \rivexam_multilevel_open:
}{
    \rivexam_multilevel_close:
}

\cs_new_nopar:Nn \rivexam_multilevel_open: {
    \par
    \hrule height 0pt% to avoid interlineskip and other vspaces
    \int_compare:nNnT {\l_rivexam_multilevel_colcount_int} > {1} {
        \dim_set:Nn \columnsep {2em}
        \dim_set:Nn \columnseprule {.4pt}
        \dim_zero:N \multicolsep
        \nobreak\vskip\lineskip
        \begin{multicols}{\int_use:N\l_rivexam_multilevel_colcount_int}
            \setlist[questions,1]{leftmargin=1em}
    }
    \rivexam_nextlevel_typeset:
}

\cs_new_nopar:Nn \rivexam_multilevel_close: {
    \skip_vertical:n {-\lastskip}
    \int_compare:nNnT {\l_rivexam_multilevel_colcount_int} > {1} {
        \vfill\hrule height 0pt\relax
        \end{multicols}
    }
}

% Ensure help lines are well printed
\RequirePackage{filehook}

\AtEndOfPackageFile*{tikz}{
    \AtBeginDocument{\tikzset{laser~help~lines/.try}}
}

% Auto exam number
\tl_new:N \theexam
\tl_map_inline:Nn \jobname {
    \tl_if_in:nnF { 0123456789 } { #1 } { \tl_map_break: }
    \tl_put_right:Nn \theexam { #1 }
}
\tl_if_empty:NF \theexam {
    \tl_set:Nx \theexam { \int_to_arabic:n {\theexam} }
}


%% Fillers & Ghost

\def\studentsize{\@setfontsize\studentsize{15pt}{18pt}}
\RequirePackage{rivghost}
\def\hfillersize{\studentsize}

% Compatibilité avec les exercices du cours:
\AtEndOfPackageFile*{tikz}{
    \tikzset{ghost/.style=transparent}
}
\newcolumntype{L}{|}
\NewDocumentCommand\NL{}{\\\hline}
\NewDocumentCommand\GHOST{ s +m }{ \rivghost[\studentsize]{#2} }
\NewDocumentCommand\UGHOST{ s +m }{ \rivughost[\studentsize]{#2} }

%%% Figure on one side of question text

\RequirePackage{sidefigure}

% Customization of statistics tables
\AtBeginDocument{
    \cs_if_exist:NT \StatsTable {
        \__rivmath_define:nn { stats / table } {
            ghosts  .value_required:n = true,
            ghosts  .code:n = {
                \rivmathsetup[stats / table]{
                    headcoltype=LlL, coltype=cL, newline=\NL,
                }
                \clist_map_inline:nn { #1 } {
                    \rivmathsetup[stats / table]{ ##1/format = \GHOST{####1} }
                }
            }
        }
    }
}
