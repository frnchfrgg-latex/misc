%% (c) 2006 RIVAUD Julien
%% License: GPL 2.0 or greater
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{rivquiz}[2009/09/15 Class for tiny tests with multiple versions]

\RequirePackage{xparse}

\LoadClassWithOptions{rivexam}
\rivexamsetup{maketitle=false,ending=none}
\geometry{
    a4paper,
    hmargin=1cm,
    vmargin=0pt,
    noheadfoot,
    ignoreheadfoot,
    bindingoffset=0pt,
    heightrounded=false,
}

\AtBeginDocument{
    \parindent\z@
    \parfillskip 0pt plus 1fil
    \thispagestyle{empty}
}


\def\classname{Classe}
\def\studentname{\textsc{Nom} Pr\'enom}

\RenewDocumentCommand \date {m} {\def \rivquiz@date {#1}}
\RenewDocumentCommand \class {m} {\def \rivquiz@class {#1}}

\date{\today}
\class{}

\NewDocumentCommand \quiz { +m }
{
    \def\quiz@rule{
        \vbox to 0pt{%
            \vss
            \parindent = 0pt%
            \parskip = 0pt%
            \hbox{%
                \hskip -\oddsidemargin
                \hskip -1in% horizontal offset hardcoded in TeX
                \vrule width \paperwidth height \fboxrule
            }%
            \vss
        }%
    }
    \def\quiz@{%
        \quiz@rule
        \vfill
        \classname: \rivquiz@class\space--- \studentname: \hfiller
        \hskip 0pt plus 1fill\relax
        \rivquiz@date\par
        \vskip.8\baselineskip
        \begingroup
        \ignorespaces
        #1%
        \endgroup
        \ifhmode\par\fi
        \vskip-\lastskip
        \vfill
    }
    \def\|##1|##2|{\choice{##1}{##2}}%
    \def\choice##1##2{##1}\quiz@\quiz@
    \def\choice##1##2{##2}\quiz@\quiz@
    \quiz@rule
    \ignorespaces
}
