%% (c) 2006 RIVAUD Julien
%% License: GPL 2.0 or greater
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\RequirePackage{expl3}
\ProvidesExplClass{progression}{2015/05/16}{1.0}
                  {Typesetting a full year course plan}

\LoadClassWithOptions{rivbook}

\RequirePackage{xparse}
\RequirePackage{enumitem}
\AtBeginDocument{\setlist*[itemize]{label=$\smblksquare$}}

\RequirePackage{tikz}
\ExplSyntaxOff
\usetikzlibrary{calc}
\ExplSyntaxOn

\RequirePackage{tcolorbox}
\ExplSyntaxOff
\tcbuselibrary{raster, skins}
\ExplSyntaxOn

\geometry{
    margin=1.5cm,
    noheadfoot,
    ignoreheadfoot,
    bindingoffset=0pt
}

\colorlet{base}{black!60!white}

\AtBeginDocument{
\colorlet{frame}{base}
\colorlet{bg}{base!7!white}
\colorlet{chapnum}{bg}
\colorlet{insert}{base}
\section*{\@title}
}


\int_new:N \l__progression_depth_int

\AtBeginEnvironment{itemize}{\int_incr:N \l__progression_depth_int}

\cs_set_eq:NN \__progression_orig_enumerate:w \enumerate
\cs_set_eq:NN \__progression_orig_endenumerate:w \endenumerate
\NewDocumentCommand \__progression_enumerate:w { O{} } {
    \int_incr:N \l__progression_depth_int
    \int_compare:nNnTF \l__progression_depth_int > 1 {
        \__progression_orig_enumerate:w[#1]
    }{
        \tcbset{
            every~box/.style={
                sidebyside, lefthand~width=13em,
                spartan, boxrule=0.8pt, leftrule=4em, boxsep=0pt,
                sharp~corners, colframe=frame, colback=bg,
                halign~upper=left, halign~lower=left,
                underlay={
                    \path (frame.north~west) --
                            node[color=chapnum,font=\large\bfseries] {
                                \stepcounter{enumi}
                                \Roman{enumi}
                            }
                        (interior.south~west);
                },
                #1
            },
            vacation/.style={
                reset,
                frame~empty, interior~empty, segmentation~empty,
                before=\penalty-100\vskip 0pt plus 2em\relax,
                after=\nobreak\vskip 0pt plus 1em\relax,
                halign=center,
                coltext=insert,
                fontupper=\sffamily\bfseries,
            }
        }
        \centering
    }
}
\cs_set_eq:NN \enumerate \__progression_enumerate:w
\cs_set_protected_nopar:Npn \endenumerate {
    \int_compare:nNnTF \l__progression_depth_int > 1 {
        \__progression_orig_endenumerate:w
    }{
        \__progression_maybeclosebox:
    }
}
\cs_set_eq:NN \__progression_orig_item:w \item
\cs_set_protected_nopar:Npn \item {
    \int_compare:nNnTF \l__progression_depth_int > 1 {
        \__progression_orig_item:w
    }{
        \__progression_doitem:w
    }
}
\NewDocumentCommand \__progression_doitem:w { O{} }{
    \__progression_maybeclosebox:
    \cs_gset_eq:NN \__progression_maybesublist: \__progression_dosublist:
    \group_begin:
    \cs_set_eq:NN \__progression_maybeclosebox: \__progression_doclosebox:
    \begin{tcolorbox}[#1]
}

\cs_new_protected_nopar:Nn \__progression_doclosebox: {
    \end{tcolorbox}
    \group_end:
}
\cs_set_eq:NN \__progression_maybeclosebox: \prg_do_nothing:

\BeforeBeginEnvironment{enumerate}{\__progression_maybesublist:}
\BeforeBeginEnvironment{itemize}{\__progression_maybesublist:}

\cs_new_protected_nopar:Nn \__progression_dosublist: {
    \tcblower
    \cs_gset_eq:NN \__progression_maybesublist: \prg_do_nothing:
}
\cs_gset_eq:NN \__progression_maybesublist: \prg_do_nothing:



\AtBeginDocument{\parindent\z@}

\pagestyle{empty}


